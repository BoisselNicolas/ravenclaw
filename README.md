<img src="https://ecole-alternance.cesi.fr/wp-content/themes/cesi/static/logo/ecole-alternance.svg">

# (RE)SOURCES RELATIONNELLES
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)]()
[![forthebadge](https://forthebadge.com/images/badges/0-percent-optimized.svg)]()

[![Version](https://badge.fury.io/gh/tterb%2FHyde.svg)]()
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)]()



## Pré-Requis

 - MongoDB
 - node
 - npm


### Installation

```
git clone git@gitlab.com:BoisselNicolas/ravenclaw.git
```

Installation les paquets de notre API

```
$ cd ravenclaw/server/
$ npm install
```

Installer les paquets nécessaire à IONIC
```
$ cd ravenclaw/Cube/
$ npm install
```


## Démarrage
Démarrer l'API

```
$ cd ravenclaw/server/
$ node server.js
```

Démarrer IONIC
```
$ cd ravenclaw/Cube/
$ ionic serve
```


## Fabriqué avec

### Front-End :

<img src="https://miro.medium.com/max/352/1*rZY47WiUohIbDS1W2bmVyA.png" alt="drawing" width="20px" /> IONIC 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/1200px-Vue.js_Logo_2.svg.png" alt="drawing" width="20px" /> Vue JS 

### Back-End :

 Express <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png" alt="drawing" width="20px" />  

### BDD :

 <img src="https://www.globant.com/sites/default/files/2021-01/MongoDB_Logo_FullColorBlack_RGB-4td3yuxzjs.png" alt="drawing" width="100px" /> 

### Environnement :

<img src="https://www.armandphilippot.com/wp-content/uploads/2020/03/vs-code.jpg" alt="drawing" width="20px" /> VS Code

<img src="https://www.nicepng.com/png/full/65-653471_linux-mint-comments-linux-mint-icon-black.png" alt="drawing" width="20px" /> Linux Mint


## Versions

**Prototype :** 0.1


## Auteurs

* **BOISSEL Nicolas** 
* **LABAT Corentin**



